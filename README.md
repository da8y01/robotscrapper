# Robot (web scrapper)

Probar el funciomaniento del robot con los siguientes pasos en su respectivo orden: 

1. Tener corriendo y disponible el motor de BD MySQL (con algún cliente: comando mysql, PHP MyAdmin, etc.).

2. Impotar la BD 'medicat.sql' en MySQL.

3. Correr Chrome en modo debug para selenium: `& 'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe' --remote-debugging-port=9222 --user-data-dir='C:\ABSOLUTE\PATH\TO\garbage-selenium'`

4. Loguearse en <https://mipres.sispro.gov.co/MIPRESNOPBS/Login.aspx>: *CC72340888 - Sheva1985*

5. Estando en el folder del robot, correr el comando: `python .\RobotServicios.py`

## Problemas comunes
* Tener diferentes versiones de *chrome driver* y browser Google Chrome (u otros navegadores dado el caso). Para solucionar esto muchas veces se recomienda sincronizar la version de *chrome driver* con la que tenga actualmente el browser instalado. Ubicar y descargar la versión requerida en: <https://chromedriver.chromium.org/downloads>

## To Do
* Refactor
* Estandarizar con Excepciones los Logs a archivo y consola.
* GUI con Qt
