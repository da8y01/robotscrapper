'''
Created on 2021-04-26

MYT MEDICA S.A.S Copyright (c) Reservados todos los derechos.
 * 
 * El uso de este Software es de uso exclusivo de MYT MEDICA S.A.S Este
 * Software NO es software libre, no se permite su distribucion, modificacion, compilacion, re-uso o
 * procesos de ingenieria a la inversa sin previa autorizacion de MYT MEDICA S.A.S
 * 
 * MYT MEDICA S.A.S  tiene derecho a realizar cambios al codigo fuente sin
 * previo aviso siendo responsable de dar libre soporte y mantenimiento a este codigo.
 *  
 * Modificacion de la automatizacion Original.Para saltar los registros con aviso
 * ROBOT CON SALTO
 * Modificacion adicional con captcha
 * 
@author: MYT MEDICA S.A.S
'''

import configparser
import logging
import sys
import time

import mysql.connector
from selenium import webdriver
from selenium.common.exceptions import (ElementClickInterceptedException,
                                        ElementNotInteractableException,
                                        JavascriptException,
                                        NoSuchElementException,
                                        StaleElementReferenceException,
                                        TimeoutException,
                                        UnexpectedAlertPresentException)
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait

from funciones import Funciones

# load properties file
config = configparser.RawConfigParser()
config.read('configServicios.properties')
sys.path.append(config.get('pathEjecucion', 'pathrobot'))
# from Servicios_Jair_Luis_Alean_Mosquera.Funciones import Funciones

# log file
logging.basicConfig(filename=config.get('log', 'log.logfile'), level=logging.INFO,
                    format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
logging.info(config.get('log', 'log.infodriver'))

# DB configuration
try:
    mydb = mysql.connector.connect(
        host=config.get('seccionDB', 'db.host'),
        user=config.get('seccionDB', 'db.user'),
        passwd=config.get('seccionDB', 'db.pwd'),
        database=config.get('seccionDB', 'db.name')
    )
except (UnicodeDecodeError) as err:
    print(err.reason)
    print(config.get('msg', 'm.errorBase'))
    logging.error(config.get('msg', 'm.errorBase'))
    sys.exit(0)
except mysql.connector.Error as dberror:
    # print dberror
    print(dberror)
    if dberror.errno == 2003:
        print(dberror)
        print(config.get('msg', 'm.errorConf'))
        logging.error(config.get('msg', 'm.errorConf'))
    if dberror.errno == 1045:
        print(dberror)
        print(config.get('msg', 'm.errorUsu'))
        logging.error(config.get('msg', 'm.errorUsu'))
    if dberror.errno == 1049:
        print(dberror)
        print(config.get('msg', 'm.base'))
        logging.error(config.get('msg', 'm.base'))
    sys.exit(0)


# driver = webdriver.Firefox(executable_path=config.get( 'drive', 'd.path' ))
# driver.install_addon(r"C:\Python27\adblock.xpi", temporary=True)
chrome_options = Options()
chrome_options.add_experimental_option("debuggerAddress", "127.0.0.1:9222")
# driver = webdriver.Chrome(config.get('drive','d.path'), chrome_options=chrome_options)
driver = webdriver.Chrome(config.get(
    'drive', 'd.path'), options=chrome_options)

# logging.info(config.get('log', 'log.funciones'))
# setea el driver para utilizar la ventana principal
# p = Funciones()
# p.setDriver(driver)

logging.info(driver)
logging.info(config.get('log', 'log.funciones'))

# setea el driver para la funcion principal de espera ######################
p = Funciones()
p.setDriver(driver)

# driver.get(config.get('urls', 'url.inicial2'))
# driver.maximize_window()
# driver.set_window_position(-1200,0)

# Pantalla de login #################################
# logging.info(config.get('secc', 'secc.logeo'))

# elem = driver.find_element_by_id(config.get('login', 'lo.usu'))
# elem.send_keys(config.get('credenciales','cr.user'))

# elem = driver.find_element_by_id(config.get('login', 'lo.pwd'))
# elem.send_keys(config.get('credenciales','cr.pwd'))

# elem.send_keys(Keys.RETURN)

# wait = WebDriverWait(driver, float(config.get('timer', 't.defecto')))

# segunda pantalla#################################
# logging.info(config.get('secc', 'secc.seleccion'))
# try:
#  el = wait.until(EC.presence_of_element_located((By.ID, config.get('rol', 'r.centidad'))))
# except TimeoutException:
#    print(config.get('excepciones', 'ex.primera'))
#   logging.error(config.get('log', 'log.timeout'))

# try:
#   el = wait.until(EC.presence_of_element_located((By.ID, config.get('rol', 'r.rol'))))
# except TimeoutException:
#    print(config.get('excepciones', 'ex.primera'))
#   logging.error(config.get('log', 'log.timeout'))
# Esperar 3 minutos o 2 y medio para completar el captcha
# time.sleep( float( config.get('timer', 't.captcha') ) )
###############################################################################

driver.get('https://mipres.sispro.gov.co/MIPRESNOPBS/Inicio.aspx')

select = Select(driver.find_element_by_id(config.get('rol', 'r.centidad')))
select.select_by_visible_text(config.get('rol', 'r.eps'))

time.sleep(float(config.get('timer', 't.pantalla2')))

select = Select(driver.find_element_by_id(config.get('rol', 'r.rol')))
# el = wait.until(EC.presence_of_all_elements_located((By.ID,  config.get('rol', 'r.rol'))))
select = Select(driver.find_element_by_id(config.get('rol', 'r.rol')))
select.select_by_visible_text(config.get('rol', 'r.reco'))
driver.find_element_by_id(config.get('rol', 'r.continuar')).click()

wait = WebDriverWait(driver, float(config.get('timer', 't.defecto')))

mycursor = mydb.cursor()
mycursor.execute(config.get('query', 'q.tercera'),
                 ('Jair Luis Alean Mosquera',))
myresult = list(mycursor.fetchall())

try:
    el = wait.until(EC.presence_of_all_elements_located(
        (By.CLASS_NAME, config.get('tutela', 't.popup'))))
except TimeoutException:
    print(config.get('excepciones', 'ex.segunda'))
    logging.error(config.get('log', 'log.timeout'))

time.sleep(float(config.get('timer', 't.primerpantalla')))

# ele2  = driver.find_element_by_class_name(config.get('tutela', 't.popup'))
#
# Hover = ActionChains(driver).move_to_element(ele2)
# Hover.perform()
#
# parent = driver.find_element_by_id(config.get('tutela', 't.menu1'))
# el= parent.find_element_by_class_name(config.get('tutela', 't.level'))
# el.click()
#
# time.sleep( float( config.get('timer', 't.primerpantalla') ) )

for fila in myresult:
    # driver.get('https://mipres.sispro.gov.co/MIPRESNOPBS/Inicio.aspx')

    driver.execute_script(
        'document.getElementById("Menu1_mnuPrincipal:submenu:2").style="display:block;"')
    time.sleep(float(config.get('timer', 't.codigo')))

    # driver.find_element_by_class_name(config.get('tutela', 't.level')).click()
    # driver.find_element_by_class_name(config.get('tutela', 't.level2')).click()

    try:
        el = wait.until(EC.presence_of_all_elements_located(
            (By.CLASS_NAME, config.get('tutela', 't.popup'))))
    except TimeoutException:
        print(config.get('excepciones', 'ex.segunda'))
        logging.error(config.get('log', 'log.timeout'))

    el = driver.find_element_by_class_name(config.get('tutela', 't.popup'))
    Hover = ActionChains(driver).move_to_element(el)
    Hover.perform()

    parent = driver.find_element_by_id(config.get('tutela', 't.menu1'))
    el = parent.find_element_by_class_name(config.get('tutela', 't.level'))
    el.click()

    # driver.execute_script("__doPostBack('ctl00$Menu1$mnuPrincipal','223\\224')")
    time.sleep(float(config.get('timer', 't.primerpantalla')))
    # loggin del inicio del cliclo#############
    logging.info(config.get('fin', 'fin.inicio'))
    # informacion del registro
    logging.info('registro a procesar:'+fila[4])
    logging.info('numero de recobro:'+fila[1])

    # cuarta pantalla ###########################################
    ###########################################################################
    logging.info(config.get('secc', 'secc.cuarta'))

    wait = WebDriverWait(driver, float(config.get('timer', 't.defecto')))
    try:
        element = wait.until(EC.presence_of_all_elements_located(
            (By.ID, config.get('datosIniciales', 'd.tipoDoc'))))
    except TimeoutException:
        print(config.get('excepciones', 'ex.tercera'))
        logging.error(config.get('excepciones', 'ex.tercera'))

    try:
        element = wait.until(EC.presence_of_all_elements_located(
            (By.ID, config.get('datosIniciales', 'd.numDoc'))))
    except TimeoutException:
        print(config.get('excepciones', 'ex.cuarta'))
        logging.error(config.get('excepciones', 'ex.cuarta'))

    time.sleep(float(config.get('timer', 't.registro')))
    try:
        el = wait.until(EC.presence_of_all_elements_located(
            (By.ID, config.get('datosIniciales', 'd.tipoDoc'))))
    except TimeoutException:
        print(config.get('excepciones', 'ex.cuarta'))
        logging.error(config.get('excepciones', 'ex.cuarta'))

    select = Select(driver.find_element_by_id(
        config.get('datosIniciales', 'd.tipoDoc')))

    if fila[3] == 'RC':  # registro civil
        select.select_by_value('1')
    if fila[3] == 'TI':  # ti
        select.select_by_value('2')
    if fila[3] == 'CC':  # ceddula
        select.select_by_value('3')
    if fila[3] == 'CE':  # cc de extranjeria
        select.select_by_value('4')
    if fila[3] == 'PA':  # pasaporte
        select.select_by_value('5')
    if fila[3] == 'NV':  # certificado de nacido vido
        select.select_by_value('6')
    if fila[3] == 'CD':  # carne diplomatico (ya)
        select.select_by_value('7')
    if fila[3] == 'SC':  # salvo conducto de permanencia
        select.select_by_value('8')
    if fila[3] == 'PR':  # pasaporte de la ONU
        select.select_by_value('9')
    if fila[3] == 'PE':  # permiso especial de permanencia
        select.select_by_value('11')

    p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
        'datosIniciales', 'd.main'), config.get('excepciones', 'ex.quinta'))
    # time.sleep(float(config.get('timer', 't.dos')))
    time.sleep(float(3))

    elem = driver.find_element_by_id(config.get('datosIniciales', 'd.numDoc'))
    elem.send_keys(fila[4])
    time.sleep(float(3))
    # driver.find_element_by_id(config.get('datosIniciales','d.lblNumeroDocumento')).click()
    # time.sleep(float(2))
    # p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
    #     'datosIniciales', 'd.main'), config.get('excepciones', 'ex.sexta'))

    # const int timeoutSeconds = 15;
    # var ts = new TimeSpan(0, 0, timeoutSeconds);
    # var wait = new WebDriverWait(webDriver, ts);

    # wait.Until((driver) => driver.FindElements(By.Id("my-id")).Count > 0);
    # webDriver.FindElement(By.Id("my-id")).Click();

    # el1 = driver.find_element_by_id(config.get('datosIniciales', 'd.mensajeerror'))
    # el2 = driver.find_element_by_id(config.get('datosIniciales', 'd.txtPrimerApellido')).get_attribute('value')
    # if el1 or el2 == '':
    #     print('ERROR: Verifique el tipo y número de documento diligenciado')

    # p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
    #     'datosIniciales', 'd.main'), config.get('excepciones', 'ex.sexta'))

    elem = driver.find_element_by_id(config.get('datosIniciales', 'd.fallo'))
    elem.click()

    p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
        'datosIniciales', 'd.main'), config.get('excepciones', 'ex.septima'))

    time.sleep(float(config.get('timer', 't.dos')))

    try:
        if driver.find_element_by_id(config.get('datosIniciales', 'd.mensajeerror')).is_displayed():
            msg3 = driver.find_element_by_id(
                config.get('datosIniciales', 'd.mensajeerror'))
            msg3 = msg3.get_attribute('innerHTML')
            mycursor = mydb.cursor()
            val = (msg3, fila[1], fila[4])
            mycursor.execute(config.get('query', 'q.fallo'), val)
            mydb.commit()
            print("paso por aca")
            continue
    except NoSuchElementException:
        print("el elemento no existe pero se continua con el flujo")

    try:
        # time.sleep(0.2)
        if driver.find_element_by_class_name(config.get('datosIniciales', 'd.modal')).is_displayed():
            msg = driver.find_element_by_class_name(config.get(
                'datosIniciales', 'd.contenido')).get_attribute('innerHTML')
            el = driver.find_element_by_class_name(
                config.get('datosIniciales', 'd.confirm'))
            el.click()
            mycursor = mydb.cursor()
            val = (msg, fila[1], fila[4])
            mycursor.execute(config.get('query', 'q.fallo'), val)
            mydb.commit()
            logging.info(msg)
            continue

    except NoSuchElementException:
        print(config.get('excepciones', 'ex.comeva'))
        logging.info(config.get('excepciones', 'ex.comeva'))

    p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
        'datosIniciales', 'd.main'), config.get('excepciones', 'ex.comeva'))

    driver.find_element_by_id(config.get(
        'datosIniciales', 'd.fallo')).send_keys(fila[5])
    driver.execute_script(config.get('datosIniciales', 'd.remov1'))
    driver.find_element_by_id(config.get(
        'datosIniciales', 'd.fallotutela')).send_keys(fila[6])
    driver.find_element_by_id(config.get(
        'datosIniciales', 'd.aclarafallo')).send_keys(fila[10])
    driver.execute_script(config.get('datosIniciales', 'd.remov2'))
    driver.find_element_by_id(config.get(
        'datosIniciales', 'd.fec1')).send_keys(fila[7])

    if fila[8] == '':
        print(config.get('excepciones', 'ex.2instancia'))
    else:
        driver.execute_script(config.get('datosIniciales', 'd.remov3'))
        driver.find_element_by_id(config.get(
            'datosIniciales', 'd.fec2')).send_keys(fila[8])

    p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
        'datosIniciales', 'd.main'), config.get('excepciones', 'ex.decima'))

    # enfermedad huerfana diagnostico principal
    if fila[16] == 'NO':
        driver.find_element_by_id(config.get(
            'datosIniciales', 'd.diagPrinci')).send_keys(fila[9])
        driver.find_element_by_id(config.get(
            'datosIniciales', 'd.btBuscar')).click()
        time.sleep(float(config.get('timer', 't.dos')))
        # p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
        #     'datosIniciales', 'd.main'), config.get('excepciones', 'ex.once'))

    try:
        if fila[12] == 'NO':
            driver.find_element_by_id(config.get(
                'datosIniciales', 'd.enfermeH')).click()
            # p.excepcionPrincipal( float( config.get('timer', 't.defecto') ), config.get('datosIniciales', 'd.main') , config.get('excepciones', 'ex.once') )

        elif fila[12] == 'SI':
            driver.find_element_by_id(config.get(
                'datosIniciales', 'd.enfermeHSi')).click()
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.once'))
            time.sleep(float(config.get('timer', 't.dos')))
            driver.find_element_by_id(config.get(
                'datosIniciales', 'd.enfermeHbusqueda')).send_keys(fila[13])
            driver.find_element_by_id(config.get(
                'datosIniciales', 'd.enfermeHbuscar')).click()
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.once'))
            time.sleep(float(config.get('timer', 't.dos')))
            el = driver.find_element_by_id(
                config.get('datosIniciales', 'd.enfermeHTabla'))
            el = el.find_element_by_link_text(fila[13])
            el.click()
            time.sleep(float(config.get('timer', 't.dos')))
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.once'))
            el = driver.find_element_by_id(
                config.get('datosIniciales', 'd.enfermeHdPSi'))
            el.click()

    except ElementClickInterceptedException as eciex:
        print(eciex)
        dir(eciex)
        print(config.get('excepciones', 'ex.huerfana'))
        logging.info(config.get('excepciones', 'ex.huerfana'))
    except Exception as ex:
        print(ex)
        dir(ex)
        logging.info(config.get('excepciones', 'ex.huerfana'))

    try:
        elem = driver.find_element_by_id(
            config.get('datosIniciales', 'd.numDoc'))
        if elem.get_attribute("value") == '':
            print(config.get('excepciones', 'ex.vacio'))
            logging.info(config.get('excepciones', 'ex.vacio'))
            # se rompe el ciclo
            continue
    except StaleElementReferenceException:
        logging.info(config.get('excepciones', 'ex.vacio'))
        continue

    p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
        'datosIniciales', 'd.main'), config.get('excepciones', 'ex.doce'))

    if fila[16] == 'NO':
        wait = WebDriverWait(driver, float(
            config.get('timer', 't.enfermedades')))
        try:
            elem = wait.until(EC.presence_of_element_located(
                (By.ID, config.get('datosIniciales', 'd.tablaEnfer'))))
            msg2 = '0'
        except TimeoutException as tex:
            print(tex)
            print(config.get('excepciones', 'ex.huerfana')+fila[9])
            try:
                msg2 = driver.find_element_by_id(config.get(
                    'datosIniciales', 'd.lTablaEnfer')).get_attribute("innerHTML")
            except NoSuchElementException as nseex:
                print(nseex)
                logging.info(config.get('log', 'log.FaltaElemento'))
                logging.info(config.get('datosIniciales', 'd.lTablaEnfer'))
                continue
            except Exception as ex:
                print(ex)
                logging.info(config.get('log', 'log.FaltaElemento'))
                logging.info(config.get('datosIniciales', 'd.lTablaEnfer'))
                continue
        except Exception as ex:
            print(ex)
    else:
        msg2 = '0'

    if msg2 == config.get('msg', 'm.registros'):
        sql = config.get('query', 'q.fallo')
        mycursor = mydb.cursor()
        ms2 = config.get('msg', 'm.codigo') + \
            fila[9] + config.get('msg', 'm.codigo') + msg2
        val = (msg2, fila[1], fila[4])
        mycursor.execute(sql, val)
        mydb.commit()
        print("llego aca")

    else:
        # time.sleep(timer6)
        if fila[16] == 'NO':
            el = driver.find_element_by_class_name("rowStyle_css")
            print(fila[9])
            el = el.find_element_by_link_text(fila[9])
            try:
                el.click()
            except StaleElementReferenceException:
                logging.info(config.get('log', 'log.ElementoRancio'))
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

        # verificar. SI adres esta clickeando antes de continuar
        try:
            if driver.find_element_by_id(config.get('relacion', 'rel.quienpaga')).is_displayed():
                driver.find_element_by_id(config.get(
                    'relacion', 'rel.quienpagaADRES')).click()
        except NoSuchElementException:
            print("el elemento no existe")

        time.sleep(float(config.get('timer', 't.pantalla2')))

        # diagnosticos relacionados
        if fila[14] != '' and fila[15] != '':
            driver.find_element_by_id(config.get(
                'datosIniciales', 'd.diagRel1txt')).send_keys(fila[14])
            driver.find_element_by_id(config.get(
                'datosIniciales', 'd.diagRel1Buscar')).click()
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

            try:
                el = driver.find_element_by_id(
                    config.get('datosIniciales', 'd.diagRel1Tabla'))
            except NoSuchElementException:
                msg3 = driver.find_element_by_id(
                    config.get('datosIniciales', 'd.diagrelerror1'))
                msg3 = msg3.get_attribute('innerHTML')
                mycursor = mydb.cursor()
                val = (msg3, fila[1], fila[4])
                mycursor.execute(config.get('query', 'q.fallo'), val)
                mydb.commit()
                logging.info(config.get(
                    'excepciones', 'ex.enferRel2') + ":" + msg3)
                continue

            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

            el = el.find_element_by_link_text(fila[14])
            el.click()
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

            driver.find_element_by_id(config.get(
                'datosIniciales', 'd.diagRel2txt')).send_keys(fila[15])
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))
            driver.find_element_by_id(config.get(
                'datosIniciales', 'd.diagRel2Buscar')).click()
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

            try:
                el = driver.find_element_by_id(
                    config.get('datosIniciales', 'd.diagRel2Tabla'))
            except NoSuchElementException:
                msg3 = driver.find_element_by_id(
                    config.get('datosIniciales', 'd.diagrelerror2'))
                msg3 = msg3.get_attribute('innerHTML')
                mycursor = mydb.cursor()
                val = (msg3, fila[1], fila[4])
                mycursor.execute(config.get('query', 'q.fallo'), val)
                mydb.commit()
                logging.info(config.get(
                    'excepciones', 'ex.enferRel2') + ":" + msg3)
                continue

            ele = el.find_element_by_tag_name("a")

            # el = el.find_element_by_link_text(fila[15])
            ele.click()
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

        elif fila[14] != '':
            driver.find_element_by_id(config.get(
                'datosIniciales', 'd.diagRel1txt')).send_keys(fila[14])
            driver.find_element_by_id(config.get(
                'datosIniciales', 'd.diagRel1Buscar')).click()
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

            try:
                time.sleep(float(config.get('timer', 't.dos')))
                el = driver.find_element_by_id(
                    config.get('datosIniciales', 'd.diagRel1Tabla'))
            except NoSuchElementException:
                # msg3 = driver.find_element_by_id(
                # config.get('datosIniciales', 'd.diagrelerror1'))
                # msg3 = msg3.get_attribute('innerHTML')
                mycursor = mydb.cursor()
                # val = (msg3, fila[1], fila[4])
                val = ('NoSuchElementException', fila[1], fila[4])
                mycursor.execute(config.get('query', 'q.fallo'), val)
                mydb.commit()
                logging.info(config.get(
                    # 'excepciones', 'ex.enferRel2') + ":" + msg3)
                    'excepciones', 'ex.enferRel2') + ":" + 'NoSuchElementException')
                continue

            el = el.find_element_by_link_text(fila[14])
            el.click()
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

        try:
            elem = driver.find_element_by_id(config.get('datosIniciales', 'd.radioQuienPaga'))
            if elem.is_displayed():
                elem.click()
            time.sleep(float(3))
        except NoSuchElementException as nseex:
            print('NSEex radio QuienPaga')
            print(nseex)
        except Exception as ex:
            print('ex radio QuienPaga')
            print(ex)

        driver.find_element_by_id(config.get(
            'datosIniciales', 'd.btnSig')).click()
        p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
            'datosIniciales', 'd.main'), config.get('excepciones', 'ex.quince'))

        driver.find_element_by_id(config.get(
            'datosIniciales', 'd.btnSig')).click()
        p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
            'datosIniciales', 'd.main'), config.get('excepciones', 'ex.quince'))
        time.sleep(float(config.get('timer', 't.dos')))
        driver.find_element_by_id(config.get(
            'datosIniciales', 'd.btnSig')).click()
        p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
            'datosIniciales', 'd.main'), config.get('excepciones', 'ex.quince'))
        time.sleep(float(config.get('timer', 't.dos')))
        driver.find_element_by_id(config.get(
            'datosIniciales', 'd.btnSig')).click()
        p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
            'datosIniciales', 'd.main'), config.get('excepciones', 'ex.quince'))
        time.sleep(float(config.get('timer', 't.dos')))
        driver.find_element_by_id(config.get(
            'datosIniciales', 'd.btnSig')).click()
        p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
            'datosIniciales', 'd.main'), config.get('excepciones', 'ex.quince'))
        time.sleep(float(config.get('timer', 't.dos')))

        try:
            if driver.find_element_by_id(config.get('relacion', 'rel.msgfinalfallo')).is_displayed():
                continue
        except NoSuchElementException:
            print("no existe el elemento pero se contiua con la ejecucion")

        p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
            'datosIniciales', 'd.main'), config.get('excepciones', 'ex.quince'))

        # driver.find_element_by_id(config.get('tutela', 't.comun')).click()
        driver.find_element_by_id(config.get('tutela2', 't.comun')).click()

        #################################
        # A partir del codigo de recobro se empieza a incluir los servicios que se tengan
        # apartir de aqui solo se procesa comeva y los avisos van a ir a la base de datos
        ##################################
        # cierre de expepcion#####

        mycursor = mydb.cursor()
        adr = (fila[1], fila[4])
        mycursor.execute(config.get('query', 'q.inicial'), adr)
        res = list(mycursor.fetchall())

        bandera = 0
        for y in res:
            logging.info(config.get('fin', 'fin.items1'))
            logging.info(config.get('secc', 'secc.servicios'))
            print(config.get('secc', 'secc.servicios'))

            wait = WebDriverWait(driver, float(
                config.get('timer', 't.defecto')))
            try:
                elem = wait.until(EC.invisibility_of_element(
                    (By.ID, config.get('datosIniciales', 'd.main'))))
            except TimeoutException:
                print(config.get('excepciones', 'ex.16'))
                logging.error(config.get('excepciones', 'ex.16'))
                bandera = 1

            time.sleep(float(config.get('timer', 't.serviciosCompl')))

            try:
                driver.find_element_by_id(
                    # config.get('tutela', 't.btn')).click()
                    config.get('tutela2', 't.btn')).click()
            except NoSuchElementException:
                logging.error(config.get('excepciones', 'ex.elemento'))

            wait = WebDriverWait(driver, float(
                config.get('timer', 't.defecto')))

            try:
                elem = wait.until(EC.invisibility_of_element(
                    (By.ID, config.get('datosIniciales', 'd.main'))))
            except TimeoutException:
                print(config.get('excepciones', 'ex.17'))
                logging.error(config.get('excepciones', 'ex.17'))
                bandera = 1

            time.sleep(float(config.get('timer', 't.registro')))

            # time.sleep(float(config.get('timer', 't.registro')))
            # tipo de tutela
            print("titulo de tutela")
            print(y[10])
            print(y[11])

            try:
                driver.find_element_by_id(
                    # config.get('tutela', 't.btn')).click()
                    config.get('tutela2', 't.btn')).click()
            except NoSuchElementException:
                logging.error(config.get('excepciones', 'ex.elemento'))

            try:
                time.sleep(float(config.get('timer', 't.dos')))
                if y[11] == 'TAX':  # tasativo
                    print(y[11])
                    driver.find_element_by_id(
                        config.get('tutela2', 't.tax')).click()
                if y[11] == 'INTE':  # integral
                    driver.find_element_by_id(
                        # config.get('tutela', 't.inte')).click()
                        config.get('tutela2', 't.inte')).click()
            except NoSuchElementException:
                logging.error(config.get('excepciones', 'ex.elemento'))

            wait = WebDriverWait(driver, float(
                config.get('timer', 't.defecto')))

            try:
                if y[5] == 'Unica':  # prestacion unica
                    driver.find_element_by_id(config.get(
                        'servicio', 'ser.presUnica')).click()
                if y[5] == 'Sucesiva':  # prestacion sucesiva
                    driver.find_element_by_id(config.get(
                        'servicio', 'ser.presSucesiva')).click()
            except NoSuchElementException:
                logging.error(config.get('excepciones', 'ex.elemento'))

            try:
                driver.find_element_by_id(config.get(
                    'servicio', 'ser.cantidad')).send_keys(y[1])
                driver.find_element_by_id(config.get(
                    'servicio', 'ser.JustNoPBS')).send_keys(y[2])
                driver.find_element_by_id(config.get(
                    'servicio', 'ser.indicaciones')).send_keys(y[3])
                driver.find_element_by_id(config.get(
                    'servicio', 'ser.complementario')).send_keys(y[4])
                driver.find_element_by_id(config.get(
                    'servicio', 'ser.cantidadTratamiento')).send_keys(y[10])
            except NoSuchElementException:
                logging.error(config.get('excepciones', 'ex.elemento'))

            try:
                elem = wait.until(EC.presence_of_element_located(
                    (By.ID, config.get('servicio', 'ser.unidadTiempo'))))
            except TimeoutException:
                print(config.get('excepciones', 'ex.18'))
                logging.error(config.get('excepciones', 'ex.18'))
                bandera = 1

            try:
                # frecuencia unidad de tiempo
                if y[6] == 1:  # minuto
                    driver.execute_script(config.get('servicio', 'ser.minuto'))
                if y[6] == 2:  # hora
                    driver.execute_script(config.get('servicio', 'ser.hora'))
                if y[6] == 3:  # dia
                    driver.execute_script(config.get('servicio', 'ser.dia'))
                if y[6] == 4:  # semana
                    driver.execute_script(config.get('servicio', 'ser.semana'))
                if y[6] == 5:  # mes
                    driver.execute_script(config.get('servicio', 'ser.mes'))
                if y[6] == 6:
                    driver.execute_script(config.get('servicio', 'ser.op6'))
                if y[6] == 8:  # unica
                    driver.execute_script(config.get('servicio', 'ser.unica'))

                el = driver.find_element_by_id(
                    config.get('servicio', 'ser.unidadTiempo'))

                if el.get_attribute('value') == '-1':
                    print(config.get('excepciones', 'ex.noPaso'))
                    logging.error(config.get('excepciones', 'ex.noPaso'))
                    bandera = 1

            except (UnexpectedAlertPresentException, JavascriptException):
                print(config.get('excepciones', 'ex.entro'))
                logging.error(config.get('excepciones', 'ex.entro'))
                bandera = 1

            try:
                select = Select(driver.find_element_by_id(
                    config.get('servicio', 'ser.periodo')))
            except NoSuchElementException:
                logging.error(config.get('excepciones', 'ex.elemento'))

            if y[7] == 1:  # minuto
                select.select_by_value('1')
            if y[7] == 2:  # hora
                select.select_by_value('2')
            if y[7] == 3:  # dia
                select.select_by_value('3')
            if y[7] == 4:  # semana
                select.select_by_value('4')
            if y[7] == 5:  # mes
                select.select_by_value('5')
            if y[7] == 6:  # anio
                select.select_by_value('6')

            select = Select(driver.find_element_by_id(
                config.get('servicio', 'ser.serComplementario')))
            # tipo de servicios complementarios
            select.select_by_visible_text(y[8])
            if y[8] == 22:  # Tranporte no ambulacia
                select.select_by_value('22')
            if y[8] == 23:  # traslados y tiquetes aereos
                select.select_by_value('23')
            if y[8] == 45:
                select.select_by_value('45')
            try:
                driver.find_element_by_id(config.get(
                    'servicio', 'ser.cantidadTotal')).send_keys(y[1])
                driver.find_element_by_id(config.get(
                    'servicio', 'ser.frecuencia')).send_keys(y[9])

            except ElementNotInteractableException:
                print(config.get('excepciones', 'ex.salto'))
                logging.error(config.get('excepciones', 'ex.salto'))
                bandera = 1

            # Verificar si no sale el div de campos requeridos, para cancerlar la accion

            # fin ciclo
            #######################################
            # frecuencia Unidad de tiempo de 1 a 6
            # Incluye el 8
            #######################################
            # select = driver.find_element_by_id('CPH_Contenido_Ses1_Cnf2_ddlUnidadTiempo')
            # if y[8] == '':
            driver.find_element_by_id(config.get(
                'servicio', 'ser.btnComplentario')).click()
            logging.info(config.get('fin', 'fin.items2'))
            time.sleep(float(config.get('timer', 't.registro')))

            try:
                if driver.find_element_by_id(config.get('servicio', 'ser.divError')).is_displayed():
                    bandera = 1

                    logging.error(config.get(
                        'excepciones', 'ex.camporequerido'))
                    msg4 = driver.find_element_by_id(
                        config.get('servicio', 'ser.divError'))
                    msg4 = msg4.get_attribute('innerHTML')
                    print(config.get('excepciones', 'ex.camporequerido'))
                    mycursor = mydb.cursor()
                    val = (msg4, fila[1], fila[4])
                    mycursor.execute(config.get('query', 'q.fallo'), val)
                    mydb.commit()

            except NoSuchElementException:
                print(config.get('excepciones', 'ex.continuar'))
                # fin ciclo
            time.sleep(float(config.get('timer', 't.datos')))
            # fin del ciclo

        if bandera == 0:
            # no hubo errores de procesamiento para los items
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.quince'))

            driver.find_element_by_id(config.get(
                'servicio', 'ser.siguiente')).click()

            wait = WebDriverWait(driver, float(
                config.get('timer', 't.defecto')))
            try:
                el = wait.until(EC.presence_of_element_located(
                    (By.ID, config.get('relacion', 'rel.busqueda'))))
            except TimeoutException:
                print(config.get('excepciones', 'ex.20'))
                logging.error(config.get('excepciones', 'ex.20'))

            driver.find_element_by_id(config.get(
                'relacion', 'rel.busqueda')).send_keys(fila[9])
            driver.find_element_by_id(config.get(
                'relacion', 'rel.btnBuscar')).click()

            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.21'))

            wait = WebDriverWait(driver, float(
                config.get('timer', 't.defecto')))
            try:
                elem = wait.until(EC.visibility_of_element_located(
                    (By.ID, config.get('relacion', 'rel.tabla'))))
            except TimeoutException:
                print(config.get('excepciones', 'ex.tabla'))
                logging.error(config.get('excepciones', 'ex.tabla'))

            el = driver.find_element_by_id(config.get('relacion', 'rel.tabla'))
            el = el.find_element_by_link_text(fila[9])
            el.click()
            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

            wait = WebDriverWait(driver, float(
                config.get('timer', 't.defecto')))
            try:
                el = wait.until(EC.visibility_of_element_located(
                    (By.ID, config.get('relacion', 'rel.criterios'))))
            except TimeoutException:
                print(config.get('excepciones', 'ex.23'))
                logging.error(config.get('excepciones', 'ex.23'))

            # para enfermedades relacionadas

            if fila[14] != '' and fila[15] != '':
                driver.find_element_by_id(config.get(
                    'relacion', 'rel.diagRel1')).send_keys(fila[14])
                driver.find_element_by_id(config.get(
                    'relacion', 'rel.diagRel1Buscar')).click()
                p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                    'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))
                el = driver.find_element_by_id(
                    config.get('relacion', 'rel.diagRel1Tabla'))
                el = el.find_element_by_link_text(fila[14])
                el.click()
                p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                    'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

                driver.find_element_by_id(config.get(
                    'relacion', 'rel.diagRel2')).send_keys(fila[15])
                p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                    'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))
                driver.find_element_by_id(config.get(
                    'relacion', 'rel.diagRel2Buscar')).click()
                p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                    'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

                time.sleep(float(config.get('timer', 't.registro')))

                el = driver.find_element_by_id(
                    config.get('relacion', 'rel.diagRel2Tabla'))
                el = el.find_element_by_tag_name("a")
                el.click()
                p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                    'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))

            elif fila[14] != '':
                time.sleep(float(config.get('timer', 't.dos')))
                driver.find_element_by_id(config.get(
                    'relacion', 'rel.diagRel1')).send_keys(fila[14])
                driver.find_element_by_id(config.get(
                    'relacion', 'rel.diagRel1Buscar')).click()
                p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                    'datosIniciales', 'd.main'), config.get('excepciones', 'ex.catorce'))
                time.sleep(float(config.get('timer', 't.dos')))
                el = driver.find_element_by_id(
                    config.get('relacion', 'rel.diagRel1Tabla'))
                el = el.find_element_by_link_text(fila[14])
                el.click()

            p.excepcionPrincipal(float(config.get('timer', 't.defecto')), config.get(
                'datosIniciales', 'd.main'), config.get('excepciones', 'ex.24'))
            time.sleep(float(config.get('timer', 't.dos')))
            driver.find_element_by_id(config.get(
                'relacion', 'rel.justMedica')).send_keys(fila[11])

            if driver.find_element_by_id(config.get('relacion', 'rel.justMedica')).get_attribute("value") == '':
                continue

            driver.find_element_by_id(config.get(
                'relacion', 'rel.criterio1')).click()
            driver.find_element_by_id(config.get(
                'relacion', 'rel.criterio2')).click()
            driver.find_element_by_id(config.get(
                'relacion', 'rel.criterio3')).click()
            driver.find_element_by_id(config.get(
                'relacion', 'rel.criterio4')).click()

            time.sleep(float(config.get('timer', 't.datos3')))
            driver.find_element_by_id(config.get(
                'relacion', 'rel.guardar')).click()
            time.sleep(float(config.get('timer', 't.datos4')))

            # driver.find_element_by_class_nameswal2-confirm styled
            # esperar hasta que salga el div
            # swal2-modal show-swal2 visible

            driver.find_element_by_class_name(
                config.get('codpress', 'cod.btn')).click()
            time.sleep(float(config.get('timer', 't.primerpantalla')))
            # esperrar  3 a  4 segundos para obtener este codigo
            codigo = driver.find_element_by_id(
                config.get('codpress', 'cod.codigo'))
            logging.info("codmipress:")
            logging.info(codigo)
            # verificar si los elementos estan cargados, si no quedan en el log
            wait = WebDriverWait(driver, float(
                config.get('timer', 't.defecto')))
            bandera = 0
            try:
                element = wait.until(EC.presence_of_all_elements_located(
                    (By.ID, config.get('codpress', 'cod.elem'))))
                bandera = 1
            except TimeoutException:
                print(config.get('excepciones', 'ex.tercera'))
                logging.error(config.get('excepciones', 'ex.tercera'))

            codigo = codigo.get_attribute("innerHTML")
            mycursor = mydb.cursor()
            val = (codigo, fila[1], fila[4])

            mycursor.execute(config.get('query', 'q.segunda'), val)
            mydb.commit()
            ################################
            logging.info(config.get('fin', 'fin.ciclo'))

#     try:
#         el = wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, config.get('tutela', 't.popup'))))
#     except TimeoutException:
#         print(config.get('excepciones', 'ex.segunda'))
#         logging.error(config.get('log', 'log.timeout'))
#
#     time.sleep( float( config.get('timer', 't.primerpantalla') ) )
#
#     ele2  = driver.find_element_by_class_name(config.get('tutela', 't.popup'))
#
#     Hover = ActionChains(driver).move_to_element(ele2)
#     Hover.perform()
#
#     parent = driver.find_element_by_id(config.get('tutela', 't.menu1'))
#
#     el= parent.find_element_by_class_name(config.get('tutela', 't.level'))
#     el.click()
#
#     time.sleep( float( config.get('timer', 't.primerpantalla') ) )

    driver.get(config.get('urls', 'url.registro'))

print(config.get('fin', 'fin.msg'))
logging.info(config.get('fin', 'fin.msg'))
mydb.close()
