'''
Created on 8 nov. 2018

MYT MEDICA S.A.S Copyright (c) Reservados todos los derechos.
 * 
 * El uso de este Software es de uso exclusivo de MYT MEDICA S.A.S Este
 * Software NO es software libre, no se permite su distribucion, modificacion, compilacion, re-uso o
 * procesos de ingenieria a la inversa sin previa autorizacion de MYT MEDICA S.A.S
 * 
 * MYT MEDICA S.A.S  tiene derecho a realizar cambios al codigo fuente sin
 * previo aviso siendo responsable de dar libre soporte y mantenimiento a este codigo.
 * 

@author: Leandro Garcia Villamil
'''

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import logging
# import ConfigParser
import configparser

config = configparser.RawConfigParser()
# config.read('config.properties')
config.read('configServicios.properties')

logging.basicConfig(filename=config.get('log', 'log.logfile'), level=logging.INFO,
                    format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


class Funciones:
    def principal(self, a, b, c):
        print("entro")

    def setDriver(self, driver):
        self.driver = driver

    def excepcionPrincipal(self, tiempo, ventana, excepnombre):
        wait = WebDriverWait(self.driver, float(tiempo))
        try:
            elem = wait.until(EC.invisibility_of_element((By.ID, ventana)))
        except TimeoutException:
            print(config.get('excepciones', excepnombre))
            logging.error(config.get('excepciones', 'ex.principal'))
